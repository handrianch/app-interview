<h1 align="center">:octocat: App Interview :octocat:</h1>

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Requirements](#requirements)
- [How to Run](#how-to-run)
- [Testing Application](#testing-application)
- [Screenshoots](#screenshoot)
- [Contributors](#contributors)

## Introduction
App Interview is an application that is used to interview prospective employees online. In this app you given several type of question, including Text Field, Multiple Choice, Multiple Select, and VideoCall. Each question have time countdown, when time countdown expired automatically proceed to the next question.


## Features
* User can register to start interview
* User can get multiple type of question, including text, multiple choice, multiple select and video

## Requirements
* [`nodejs`](https://nodejs.org)
* [`npm`](https://www.npmjs.com/get-npm)
* [`react native`](https://facebook.github.io/react-native)
* [`redux`](https://redux.js.org/)
* [`react navigation`](https://reactnavigation.org/)
* [`react native camera`](https://github.com/react-native-community/react-native-camera)


## How To Run

1. Clone this repository
   ```
   $ git clone https://github.com/handrianch/app-interview.git
   ```
2. Install all depedencies on the package.json
   ```
   $ cd app-interview
   $ npm install
   ```
3. Run Application
   ```
   $ react-native start
   $ react-native run-android or react-native run-ios
   ```
## Testing Application
If you want to try this app, you cant download directly to this link <a href="http://bit.ly/30S5PDy">AppInterview.apk</a>. After succesfully downloaded, you can install on your own smartphone.

## Screenshoot
<div align="center">
    <img width="200" src="https://github.com/handrianch/app-interview/blob/master/screenshoots/register.jpeg">
    <img width="200" src="https://github.com/handrianch/app-interview/blob/master/screenshoots/text.jpeg">
    <img width="200" src="https://github.com/handrianch/app-interview/blob/master/screenshoots/multiple-select.jpeg">
    <img width="200" src="https://github.com/handrianch/app-interview/blob/master/screenshoots/multiple-choice.jpeg">
    <img width="200" src="https://github.com/handrianch/app-interview/blob/master/screenshoots/video.jpeg">
    <img width="200" src="https://github.com/handrianch/app-interview/blob/master/screenshoots/camera.jpeg">
    <img width="200" src="https://github.com/handrianch/app-interview/blob/master/screenshoots/finish.jpeg">
</div>


## Contributors
<center>
  <table>
    <tr>
      <td align="center">
        <a href="https://github.com/handrianch">
          <img width="100" src="https://avatars3.githubusercontent.com/u/34035457?s=460&v=4" alt="Candra Handrian"><br/>
          <sub><b>Candra Handrian</b></sub>
        </a>
      </td>
    </tr>
  </table>
</center>
