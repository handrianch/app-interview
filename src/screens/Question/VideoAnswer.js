import React, { Component } from 'react'
import { View, Text, ActivityIndicator } from 'react-native'
import { Button, Container, Content, Grid, Col } from 'native-base';
import { connect } from 'react-redux'
import * as actionAnswer from '../../redux/actions/answer'
import * as actionQuestion from '../../redux/actions/question'
import CameraScreen from './CameraScreen'
import {withNavigation} from 'react-navigation'

class VideoAnswer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      showButton: false,
    };
  }

  handleOpenCamera = () => this.props.navigation.navigate("Camera")

  handleNextAnswer = () => {
    this.setState({isLoading: true})
    this.props.sendAnswer(this.props.video.payload)
  }

  payloadEmpty = payload => {
    for(let key in payload) {
      if(payload.hasOwnProperty(key)) {
        return false;
      }
    }
    return true
  }

  render() {
    if(this.props.answer.isSuccess) {
      this.props.getQuestion(this.props.question.page)
      this.setState({answer: ''})
      this.props.answer.isSuccess = false
    }

    if(this.props.question.isSuccess) {
      this.setState({timer: this.props.question.timer, isLoading: false})
      this.props.question.isSuccess = false
    }

    if(this.props.video.payload) {
      if(!this.payloadEmpty(this.props.video.payload) && !this.state.showButton) {
        this.setState({showButton: true})
      }
    }

    return (
      <Container>
        <Content>
          <View style={{ alignSelf: 'center', marginBottom: 15}}>
            <Button info rounded style={{ padding: 10, selfAlign: 'center' }} onPress={this.handleOpenCamera}>
              <Text style={{ color: '#fff' }}>Open Camera</Text>
            </Button>
          </View>
          {
            this.state.showButton ?
              <Button block primary onPress={this.handleNextAnswer}>
              {
                this.state.isLoading ?
                  <ActivityIndicator color="whitesmoke" size="small" />
                  :
                  <Text style={{ color: '#fff' }}>Next Answer</Text>
              }
              </Button>
            :
              <Button block primary disabled>
                  <Text style={{ color: '#fff' }}>Next Answer</Text>
              </Button>
          }
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    answer: state.answer,
    question: state.question,
    register: state.register,
    video: state.video
  };
};

const mapDispatchToProps = dispatch => {
  return {
    sendAnswer: newValue => dispatch(actionAnswer.sendAnswer(newValue)),
    getQuestion: page => dispatch(actionQuestion.getQuestion(page)),
  };
};

export default withNavigation(connect(mapStateToProps,mapDispatchToProps)(VideoAnswer));