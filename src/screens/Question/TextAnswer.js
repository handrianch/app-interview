import React, { Component } from 'react'
import { View, TextInput, Text, ActivityIndicator } from 'react-native'
import { Container, Content, Form, Item, Label, Textarea, Button } from 'native-base';
import { connect } from 'react-redux'
import * as actionAnswer from '../../redux/actions/answer'
import * as actionQuestion from '../../redux/actions/question'
import CountDown from 'react-native-countdown-component';

class TextAnswer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      answer: '',
      timer: 0,
      isLoading: false
    };
  }

  handleNextAnswer = () => {
    this.setState({isLoading: true})
    this.props.sendAnswer({
      question_id: this.props.question.data.id,
      user_id: this.props.register.data.id,
      answer: this.state.answer
    })
  }

  handleAnswers = text => this.setState({answer: text})

  handlingTimeout = () => {
    alert("Waktu Habis")
    this.props.getQuestion(this.props.question.page)
  }

  render() {
    if(this.props.answer.isSuccess) {
      this.props.getQuestion(this.props.question.page)
      this.props.answer.isSuccess = false
    }

    if(this.props.question.isSuccess) {
      this.setState({timer: this.props.question.timer, isLoading: false})
      this.props.question.isSuccess = false
    }

    return (
      <View>
        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginRight: 25}}>
          <Text style={{ textAlign: 'center', fontSize: 12 }}>Timer : </Text>
            <CountDown
              until={this.props.question.timer}
              onFinish={() => this.handlingTimeout()}
              size={12}
              timeToShow={['M', 'S']}
            />
         </View>
         <Form style={{ marginBottom: 20 }}>
            <Textarea
              rowSpan={5}
              bordered
              placeholder="Type Here.."
              value={this.state.answer}
              onChangeText={text => this.handleAnswers(text)}
            />
          </Form>
        {
          this.state.answer.trim() == '' ?
          <Button block primary disabled>
              <Text style={{ color: '#fff' }}>Next Answer</Text>
          </Button>
          :
          <Button block primary onPress={this.handleNextAnswer}>
            {
              this.state.isLoading ?
              <ActivityIndicator color="whitesmoke" size="small" />
              :
              <Text style={{ color: '#fff' }}>Next Answer</Text>
            }
          </Button>
        }

      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    answer: state.answer,
    question: state.question,
    register: state.register
  };
};

const mapDispatchToProps = dispatch => {
  return {
    sendAnswer: newValue => dispatch(actionAnswer.sendAnswer(newValue)),
    getQuestion: page => dispatch(actionQuestion.getQuestion(page)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TextAnswer);