import React, { Component } from 'react'
import { View, Text, BackHandler } from 'react-native'
import { Container, Grid, Col, Button } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import RNExitApp from 'react-native-exit-app';

class FinalScreen extends Component {
  exitApp = () => {
    RNExitApp.exitApp();
  }

  render() {
    return (
      <Container style={{ justifyContent: 'center'}}>
        <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={{ flex: 1 }}>
          <Grid>
            <Col style={{ justifyContent: 'center', alignItems: 'center', paddingHorizontal: 15 }}>
              <Text style={{ color: 'whitesmoke', fontSize: 20, fontWeight: 'bold' }}>Terimakasih</Text>
              <Text style={{ color: 'whitesmoke', fontSize: 17, fontWeight: 'bold' }}>Kami akan menguhubungi anda secepatnya</Text>
              <Button block primary style={{ marginTop: 15}} onPress={this.exitApp}>
                <Text style={{ color: 'whitesmoke' }}>Ok</Text>
              </Button>
            </Col>
          </Grid>
        </LinearGradient>
      </Container>
    )
  }
}

export default FinalScreen