import React, { Component } from 'react'
import { View, Text, Button, ActivityIndicator, Alert, StatusBar } from 'react-native'
import { Container, Content, Grid, Col } from 'native-base';
import { connect } from 'react-redux'
import * as action from '../../redux/actions/question'
import TextAnswer from './TextAnswer'
import VideoAnswer from './VideoAnswer'
import MultipleSelectAnswer from './MultipleSelectAnswer'
import MultipleChoiceAnswer from './MultipleChoiceAnswer'
import FinalScreen from './FinalScreen'
import LinearGradient from 'react-native-linear-gradient';

class Question extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      isSucces: true,
      isFinished: false
    };
  }

  componentDidMount() {
    this.props.getQuestion(this.props.question.page)
  }

  navigateToHome = () => {
    alert("Terimakasih...")
    this.props.navigation.navigate("FinalScreen")
  }

  render() {
    if(this.props.question.isLoading) {
      this.setState({isLoading: true})
    } else if(this.props.question.isSuccess) {
      this.setState({isLoading: false, isSucces: true})
      if(this.props.question.data.length == 0) {
        alert('Terimakasih')
        this.navigateToHome()
      }
      this.props.question.isSuccess = false
    }

    return (
      <Container style={{ justifyContent: 'center'}}>
        <StatusBar backgroundColor="#4c669f" barStyle="light-content" />
        <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={{ flex: 1 }}>
          <Content>
            <View style={{ flex: 1, height: 250, paddingHorizontal: 15 }}>
              <Grid style={{ marginBottom: 10, marginTop: '30%' }}>
                <Col>
                  <Text style={{ fontWeight: 'bold', fontSize: 25, color: '#fafafa', fontFamily: 'Roboto', textTransform: 'uppercase', textAlign: 'center' }}>
                    PT. Oke Oke Aja
                  </Text>
                </Col>
              </Grid>
              <Grid style={{ marginBottom: 20, backgroundColor: '#ececec', padding: 20, borderRadius: 15 }}>
                <Col style={{ justifyContent: 'center' }}>
                  <Text style={{ fontWeight: 'bold', fontSize: 15 }}>
                    {this.props.question.data && this.props.question.data.description}
                  </Text>
                </Col>
              </Grid>
            </View>
            <View style={{ backgroundColor: '#fff', borderTopLeftRadius: 30, borderTopRightRadius: 30, paddingTop: 10, paddingHorizontal: 15, height: 365}}>
              {this.props.question.data && this.props.question.data.type == 'text' && <TextAnswer />}
              {this.props.question.data && this.props.question.data.type == 'multiple select' && <MultipleSelectAnswer />}
              {this.props.question.data && this.props.question.data.type == 'multiple choice' && <MultipleChoiceAnswer />}
              {this.props.question.data && this.props.question.data.type == 'video' && <VideoAnswer />}
            </View>
          </Content>
        </LinearGradient>
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    question: state.question
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getQuestion: page => dispatch(action.getQuestion(page)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Question);