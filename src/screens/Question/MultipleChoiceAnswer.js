import React, { Component } from 'react'
import { View, Text, ActivityIndicator } from 'react-native'
import { Button } from 'native-base';
import { connect } from 'react-redux'
import * as actionAnswer from '../../redux/actions/answer'
import * as actionQuestion from '../../redux/actions/question'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import CountDown from 'react-native-countdown-component';

class MultipleChoiceAnswer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      choice: [],
      answer: 1,
      timer: 0,
      isLoading: false
    };
  }

  componentDidMount() {
    const arr = this.props.question.data.options.split(",");
      const choice = arr.map((item, index) => {
        return {label: item, value: index}
      })
      this.setState({choice})
  }

  handleNextAnswer = () => {
    this.setState({isLoading: true})
    this.props.sendAnswer({
      question_id: this.props.question.data.id,
      user_id: this.props.register.data.id,
      answer: this.state.answer
    })
  }

  handleChangeRadio = value => this.setState({answer: value + 1})

  handlingTimeout = () => {
    alert("Waktu Habis")
    this.props.getQuestion(this.props.question.page)
  }

  render() {
    if(this.props.question.isSuccess) {
      const arr = this.props.question.data.options.split(",");
      const choice = arr.map((item, index) => {
        return {label: item, value: index}
      })
      this.setState({choice, isLoading: false})
      this.props.question.isSuccess = false
    }

    if(this.props.answer.isSuccess) {
      this.props.getQuestion(this.props.question.page)
      this.props.answer.isSuccess = false
    }

    return (
      <View>
        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginRight: 25}}>
          <Text style={{ textAlign: 'center', fontSize: 12 }}>Timer : </Text>
            <CountDown
              until={this.props.question.timer}
              onFinish={() => this.handlingTimeout()}
              size={12}
              timeToShow={['M', 'S']}
            />
         </View>

        <RadioForm
          radio_props={this.state.choice}
          initial={0}
          onPress={(value) => this.handleChangeRadio(value)}
        />
        {
          this.state.answer.length == 0 ?
            <Button block primary disabled>
                <Text style={{ color: '#fff' }}>Get In</Text>
            </Button>
            :
            <Button block primary onPress={this.handleNextAnswer} style={{ marginTop: 10 }}>
            {
              this.state.isLoading ?
                <ActivityIndicator color="whitesmoke" size="small" />
                :
                <Text style={{ color: '#fff' }}>Next Answer</Text>
            }
            </Button>
        }
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    answer: state.answer,
    question: state.question,
    register: state.register
  };
};

const mapDispatchToProps = dispatch => {
  return {
    sendAnswer: newValue => dispatch(actionAnswer.sendAnswer(newValue)),
    getQuestion: page => dispatch(actionQuestion.getQuestion(page)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MultipleChoiceAnswer);