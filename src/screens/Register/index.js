import React, { Component, Fragment } from 'react'
import { View, ActivityIndicator, StatusBar } from 'react-native'
import { Container, Content, Form, Item, Input, Label, Button, Icon, Text, H1 } from 'native-base';
import { connect } from 'react-redux'
import * as action from '../../redux/actions/register'
import { Col, Row, Grid } from 'react-native-easy-grid';
import LinearGradient from 'react-native-linear-gradient';

class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      email: '',
      phoneNumber: '',
      isLoading: false,
      fulfilled: false,
      nameError: '',
      emailError: '',
      phoneNumberError: '',
    };
  }

  componentDidMount() {
    this.setState({
      name: '',
      email: '',
      phoneNumber: '',
    })
  }

  handleChangeName = text => {
    this.setState({name: text})
  }

  handleChangeEmail = text => {
    this.setState({email: text})
  }

  handleChangePhoneNumber = text => {
    this.setState({phoneNumber: text})
  }

  sendForm = () => {
    this.setState({isLoading: true, nameError: '', emailError: '', phoneNumberError: ''})
    this.props.postRegister({
      name: this.state.name,
      email: this.state.email,
      phone_number: this.state.phoneNumber,
    })
  }

  navigate = () => {
    this.props.navigation.navigate("Question")
  }

  render() {

    if(this.props.register.isSuccess) {
      this.navigate()
      this.setState({isLoading: false})
      this.props.register.isSuccess = false
    } else if(this.props.register.isError) {
      const errors = this.props.register.data
      errors.forEach(item => {
        if(item.field == "name") {
            this.setState({nameError: item.message})
        } else if(item.field == "email") {
            this.setState({emailError: item.message})
        } else if(item.field == "phone_number") {
            this.setState({phoneNumberError: item.message})
        }
      })
      alert('Register Failed')
      this.setState({isLoading: false})
      this.props.register.isError = false
    }

    return (
      <Container style={{ flex: 1, justifyContent: 'center'}}>
        <StatusBar backgroundColor="#4c669f" barStyle="light-content" />
        <Content>
          <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']}>
          <Grid style={{ height: 250 }}>
            <Col style={{ justifyContent: 'center' }}>
              <H1 style={{ textAlign: 'center', fontSize: 30, color: '#fafafa', textTransform: 'uppercase', fontFamily: 'MontserratBlack' }}>App Interview</H1>
            </Col>
          </Grid>
          <View style={{ backgroundColor: '#fff', borderTopLeftRadius: 30, borderTopRightRadius: 30, paddingTop: 20}}>
            <Grid>
              <Col>
                <Form style={{ paddingRight: 20, marginBottom:30 }}>
                  {
                    this.state.nameError != '' ?
                      <Fragment>
                        <Item floatingLabel error>
                          <Label>Name</Label>
                          <Input
                            value={this.state.name}
                            onChangeText={(text) => this.handleChangeName(text)} />
                            <Icon name='close-circle' />
                        </Item>
                        <Text style={{ marginLeft: 15, fontSize: 12, color: 'red' }}>{this.state.nameError}</Text>
                      </Fragment>
                      :
                      <Fragment>
                        <Item floatingLabel>
                          <Label>Name</Label>
                          <Input
                            value={this.state.name}
                            onChangeText={(text) => this.handleChangeName(text)} />
                        </Item>
                      </Fragment>
                  }

                  {
                    this.state.emailError != '' ?
                     <Fragment>
                      <Item floatingLabel error>
                        <Label>Email</Label>
                        <Input
                          keyboardType="email-address"
                          value={this.state.email}
                          onChangeText={(text) => this.handleChangeEmail(text)} />
                          <Icon name='close-circle' />
                      </Item>
                      <Text style={{ marginLeft: 15, fontSize: 12, color: 'red' }}>{this.state.emailError}</Text>
                    </Fragment>
                    :
                    <Fragment>
                      <Item floatingLabel>
                        <Label>Email</Label>
                        <Input
                          keyboardType="email-address"
                          value={this.state.email}
                          onChangeText={(text) => this.handleChangeEmail(text)} />
                      </Item>
                    </Fragment>
                  }

                  {
                    this.state.phoneNumberError != '' ?
                      <Fragment>
                        <Item floatingLabel error>
                          <Label>Phone Number</Label>
                          <Input
                            keyboardType="phone-pad"
                            value={this.state.phone_number}
                            onChangeText={(text) => this.handleChangePhoneNumber(text)} />
                            <Icon name='close-circle' />
                        </Item>
                        <Text style={{ marginLeft: 15, fontSize: 12, color: 'red' }}>{this.state.phoneNumberError}</Text>
                      </Fragment>
                      :
                      <Fragment>
                        <Item floatingLabel>
                          <Label>Phone Number</Label>
                          <Input
                            keyboardType="phone-pad"
                            value={this.state.phone_number}
                            onChangeText={(text) => this.handleChangePhoneNumber(text)} />
                        </Item>
                      </Fragment>
                  }
                </Form>
              </Col>
            </Grid>
            <Grid style={{ paddingHorizontal: 15 }}>
              <Col>
                {
                  this.state.name.trim() == '' || this.state.email.trim() == '' || this.state.phoneNumber.trim() == '' ?
                  <Button block primary disabled>
                      <Text style={{ color: '#fff' }}>Get In</Text>
                  </Button>
                :
                  <Button block primary onPress={this.sendForm} style={{ marginBottom: 5 }}>
                    {
                      this.state.isLoading ?
                        <ActivityIndicator color="whitesmoke" size="small" />
                        :
                        <Text style={{ color: '#fff' }}>Get In</Text>
                    }
                  </Button>
                }
              </Col>
            </Grid>
          </View>
          </LinearGradient>
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    register: state.register
  };
};

const mapDispatchToProps = dispatch => {
  return {
    postRegister: newUser => dispatch(action.postRegister(newUser)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Register);