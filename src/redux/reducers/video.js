import * as types from './../types'

const initialState = {
    payload: {},
}

export default (state = initialState, action) => {
  switch(action.type) {
    case types.UPLOAD_VIDEO:
      return {...state, payload: action.payload}
    default :
      return state
  }
}