import { combineReducers } from 'redux';
import { createNavigationReducer } from 'react-navigation-redux-helpers';

import RootNavigator from '../../navigations/RootNavigation';
import register from './register'
import question from './question'
import answer from './answer'
import video from './video'

const router = createNavigationReducer(RootNavigator);

const appReducer = combineReducers({
  router,
  register,
  question,
  answer,
  video
});

export default appReducer;