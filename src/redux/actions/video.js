import * as types from './../types'
import { url } from './../../config/config'

export const uploadVideoData = path => {
    return {
        type : types.UPLOAD_VIDEO,
        payload : path
    }
}