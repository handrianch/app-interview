import axios from 'axios'

import * as types from './../types'
import { url } from './../../config/config'

export const sendAnswer = newAnswer => {
    return {
        type : types.SEND_ANSWER,
        payload : axios.post(`${url.server}/api/v1/answer`, newAnswer)
    }
}